
/**
 * Proxy for console log/error/etc that will log normally but also
 * append the message to the document body and format objects nicely.
 * 
 * @param {function} origLog - The original logging function that's being proxied
 * @param {string} color - A CSS color string that will be used to style the text
 * @param {...*} args - Any additional arguments will be included in the log message
 */
const newLog = (origLog, color, ...args) => {
  origLog(...args);

  let p = document.createElement('pre');
  p.innerText = args.map(a => typeof a === 'string'? a : JSON.stringify(a, null, 2)).join(' ');
  p.style.color = color;
  document.body.appendChild(p);
}

// Rebind the window's console log and error so that they also
// print to the document body with *colors*
let _consoleLog = console.log,
  _consoleErr = console.error;
console.log = newLog.bind(null, _consoleLog, 'black');
console.error = newLog.bind(null, _consoleErr, 'red');

const deployJob = async () => {
  console.log("Deploying job...");

  let job = dcp.compute.for(1, 5, async function(n){
    progress();
    let ping = () => new Promise(resolve => setTimeout(() => {
      progress();
      resolve();
    }, 2000));

    console.log("It works!", n);
    await ping(); // Simulate doing some work
    await ping();
    await ping();
    return n * 2;
  });
    
  job.on('console', (msg) => console.log("Got console event:", msg));
  job.on('uncaughtException', (e) => console.error(e));
  // job.work.on('myFavEvent', (msg) => console.log("Got my fav event:", msg));

  job.on('accepted', () => {
    console.log("Job accepted");
    // setTimeout(async () => {
    // 	console.warn("Cancelling job.......");
    // 	console.log('Receipt:', await job.cancel());
    // }, 1500);
  });

  job.on('status', (status) => {
    console.log("Got a status update:", status);
  });

  job.on('cancel', (m) => console.log("Job was cancelled", m));
  job.on('result', (ev) => console.log("Got a result:", ev));
  let results = await job.exec(dcp.compute.marketValue);

  console.log("Done.");
  console.log("Results are:", (await results.fetch()).values());
}

const main = () => {
  document.getElementById('go-button').addEventListener('click', deployJob);
}

document.addEventListener('DOMContentLoaded', main, false);
